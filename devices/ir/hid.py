#!/env/python3

# sudo pip3 install hid-tools

import subprocess

def parse(line):
    dic = {}
    for l in line.split("|"):
        s = l.split(":")
        if len(s) == 2:
            c = s[0].strip()
            v = s[1].strip()
            dic[c] = int(v)
    return dic

    

def get_hid():
    with subprocess.Popen(["sudo", "hid-recorder", "/dev/hidraw1"], stdout=subprocess.PIPE) as proc:
        for line in proc.stdout:
            line = line.decode("utf-8")
            # l = f.readline()
            if "Confidence" in line:
                d = parse(line[65:])
                if "Contact Id" in d and d["Contact Id"] < 255:
                    print(d)

if __name__=="__main__":
    get_hid()
