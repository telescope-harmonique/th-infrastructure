#ifndef seg_h
#define seg_h

#define MAX_LEDS 216

#include <Arduino.h>
#include <OctoWS2811.h>
#include "log.h"
#include "colors.h"

class Seg{
 public:
  Seg(int leds_per_strip, int strip_num, int start, int seg_length, OctoWS2811 *leds);

  // utils
  int pos_led(int i);

  // colors
  void set_color(unsigned int hue, unsigned int sat, unsigned int val);
  void set_full_hsl(unsigned int hue, unsigned int sat, unsigned int val);
  void set_full(int col);

  // mode 1 fadeout
  void set_mode_fadeout(int val);
  boolean update_fadeout();

  // mode 2 fadein
  void set_mode_fadein(int val);
  boolean update_fadein();

  // mode 3 snake
  void set_mode_snake(int val);
  boolean update_snake();

  // mode 4 stack
  void set_mode_stack(int val);
  boolean update_stack();
  void trig_stack(int val);

  // global setup and actions
  void set_seg_num(int i);
  void set_mode(int mode, int val);
  boolean update();
  void trig(int val);

private:
  // static
  int strip_num;         // strip number
  int start;             // position on the strip
  int _seg_length;       // number of leds in the segment
  int seg_num;           // segment number

  OctoWS2811 *leds;

  // positions in the global led array
  int start_strip;
  int end_strip;

  // global variables
  int mode;              // current mode
  int val;               // duration
  boolean reverse;       // direction of the animation
  int col_table[150];    // buffer of colors intensity

  // animation
  float pos;             // current position of the animation
  unsigned long start_time;
  unsigned long last_trig;

  // fade
  int pos_col;          // color position at last update

  // snake
  int pos_pix;          // pixel position at last update

  // stack
  unsigned int timestep; // update rate
  int val_trig;          // value of the last trig
  int lum[MAX_LEDS];     // luminosity for stack
};

#endif
