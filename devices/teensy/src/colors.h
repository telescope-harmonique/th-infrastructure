#ifndef colors_h
#define colors_h

#include <Arduino.h>

unsigned int h2rgb(unsigned int v1, unsigned int v2, unsigned int hue);
int makeColor(unsigned int hue, unsigned int saturation, unsigned int lightness);

#endif
