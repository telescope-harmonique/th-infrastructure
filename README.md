Th-Infrastructure
==


## Démarrage cité des sciences

- allumer l'ordinateur central (grande tour noire) et le VP (télécommande)
- allumer les NUC (multiprise au sol avec bouton au bout des trois tables)

## Extinction 

- éteindre l'ordinateur principal
- éteindre chaque NUC en utilisant le bouton éteindre de l'ordi.

## Liens

URL: http://192.168.1.110:8000

[Accueil sketches](http://192.168.1.110:8000)

[Télécommande game master](http://192.168.1.110:8000/game-master)

