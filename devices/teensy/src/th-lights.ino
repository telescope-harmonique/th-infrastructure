/*  OctoWS2811 Rainbow.ino - Rainbow Shifting Test
    http://www.pjrc.com/teensy/td_libs_OctoWS2811.html
    Copyright (c) 2013 Paul Stoffregen, PJRC.COM, LLC

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.


  Required Connections
  --------------------
    pin 2:  LED Strip #1    OctoWS2811 drives 8 LED Strips.
    pin 14: LED strip #2    All 8 are the same length.
    pin 7:  LED strip #3
    pin 8:  LED strip #4    A 100 ohm resistor should used
    pin 6:  LED strip #5    between each Teensy pin and the
    pin 20: LED strip #6    wire to the LED strip, to minimize
    pin 21: LED strip #7    high frequency ringining & noise.
    pin 5:  LED strip #8
    pin 15 & 16 - Connect together, but do not use
    pin 4 - Do not use
    pin 3 - Do not use as PWM.  Normal use is ok.
    pin 1 - Output indicating CPU usage, monitor with an oscilloscope,
            logic analyzer or even an LED (brighter = CPU busier)
*/

#include <OctoWS2811.h>

// CmdParser Headers
#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>

#include "log.h"
#include "colors.h"
#include "seg.h"

#define LEDS_PER_STRIP 206

boolean b_update = false;

// OctoWS2811 Init ////////////////////////

DMAMEM int displayMemory[LEDS_PER_STRIP*6];
int drawingMemory[LEDS_PER_STRIP*6];

const int config = WS2811_GRB | WS2811_800kHz;

OctoWS2811 leds(LEDS_PER_STRIP, displayMemory, drawingMemory, config);


// Led segments init //////////////////////

#define NUM_SEGS 9

Seg segs[] = {
  Seg(LEDS_PER_STRIP, 0, 0, 130, &leds),
  Seg(LEDS_PER_STRIP, 0, 130, 60, &leds),
  Seg(LEDS_PER_STRIP, 0, 190, 16, &leds),
  Seg(LEDS_PER_STRIP, 1, 0, 130, &leds),
  Seg(LEDS_PER_STRIP, 1, 130, 60, &leds),
  Seg(LEDS_PER_STRIP, 1, 190, 16, &leds),
  Seg(LEDS_PER_STRIP, 2, 0, 130, &leds),
  Seg(LEDS_PER_STRIP, 2, 130, 60, &leds),
  Seg(LEDS_PER_STRIP, 2, 190, 16, &leds)
};


// CmdParser Init /////////////////////////

CmdCallback<4> cmdCallback;

CmdBuffer<32> myBuffer;
CmdParser     myParser;

char strCol[] = "col";
char strTrig[] = "trig";
char strSet[]  = "set";
char strMode[] = "mode";

int val;

// Callback init //////////////////////////

void functCol(CmdParser *myParser){
  if (myParser->equalCmdParam(0, "col")) {
    int h = String(myParser->getCmdParam(1)).toInt();
    int s = String(myParser->getCmdParam(2)).toInt();
    int l = String(myParser->getCmdParam(3)).toInt();

    int col = makeColor(h, s, l);

    for (int i=0; i<6*LEDS_PER_STRIP;i++){
      leds.setPixel(i, col);
    }
    leds.show();

  } else {
    warningln("Only set is allowed!");
  }
}

void functTrig(CmdParser *myParser) {
  if (myParser->equalCmdParam(0, "trig")) {
    int seg_num = String(myParser->getCmdParam(1)).toInt();
    val = String(myParser->getCmdParam(2)).toInt();

    if (seg_num < NUM_SEGS){
      segs[seg_num].trig(val);
    }else{
      warningln("i_seg > num_segs");
    }
  }
}

void functSet(CmdParser *myParser)
{
  if (myParser->equalCmdParam(0, "set")) {
    int seg_num = String(myParser->getCmdParam(1)).toInt();
    int hue = String(myParser->getCmdParam(2)).toInt();
    int saturation = String(myParser->getCmdParam(3)).toInt();
    int lightness = String(myParser->getCmdParam(4)).toInt();

    if (seg_num < NUM_SEGS){
      segs[seg_num].set_color(hue, saturation, lightness);
    }else{
      warningln("i_seg > num_segs");
    }
  }
}

void functMode(CmdParser *myParser){
  if (myParser->equalCmdParam(0, "mode")) {
    int seg_num = String(myParser->getCmdParam(1)).toInt();
    int mode = String(myParser->getCmdParam(2)).toInt();
    int val = String(myParser->getCmdParam(3)).toInt();

    if (seg_num < NUM_SEGS){
      segs[seg_num].set_mode(mode, val);
    }else{
      warningln("i_seg > num_segs");
    }
  }
}


void setup() {
  pinMode(1, OUTPUT);
  digitalWrite(1, HIGH);

  leds.begin();
  Serial.begin(256000);
  Serial.println("[teensy] serial start");

  /* myBuffer.setEcho(true); */
  for (int i=0; i<NUM_SEGS; i++){
    segs[i].set_seg_num(i);
  }

  cmdCallback.addCmd(strTrig, &functTrig);
  cmdCallback.addCmd(strSet, &functSet);
  cmdCallback.addCmd(strMode, &functMode);
  cmdCallback.addCmd(strCol, &functCol);
  digitalWrite(1, LOW);

  for (int i=0; i<6*LEDS_PER_STRIP;i++){
    leds.setPixel(i, 0x000000);
  }
  leds.show();
  Serial.println("[teensy] end setup");
}


void loop() {
  cmdCallback.updateCmdProcessing(&myParser, &myBuffer, &Serial);
  update_segs();
  if (b_update) {
    if (!leds.busy()){
      leds.show();
    }else{
      debugln("busy");
    }
  }
  delay(2);
}


void update_segs(){
  b_update = false;
  for (int i=0; i < NUM_SEGS; i++){
    b_update = b_update || segs[i].update();
  }
}
