#!/bin/sh

sudo chmod 744 devices/ir/xinput-ir.sh
sudo chmod 664 services/xinput-ir.service

sudo cp services/xinput-ir.service /etc/systemd/system

sudo systemctl daemon-reload
sudo systemctl enable xinput-ir.service

exit 0
