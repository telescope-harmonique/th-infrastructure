#!/bin/bash

rm -rf ~/snap/chromium/common/.cache

unclutter -idle 1 -root &
sleep 10
chromium-browser --start-fullscreen "http://localhost:8000/app/main"
