
#include <Arduino.h>
#include "seg.h"

Seg::Seg(int leds_per_strip, int strip_num_in, int start_in, int seg_length, OctoWS2811 *leds_in){
  // static
  strip_num = strip_num_in;
  start = start_in;
  _seg_length = seg_length;
  seg_num = 99;  // set in setup

  leds = leds_in;

  start_strip = leds_per_strip * strip_num + start_in;
  end_strip = start_strip + _seg_length;

  // global variables
  mode = 0;
  val = 500;
  reverse = false;
  for (int i=0; i < 100; i ++){
    col_table[i] = 0;
  }

  // animation
  pos = 0;
  start_time = 0;
  last_trig = millis();

  // fade
  pos_col = 0;

  // snake
  pos_pix = 0;

  // stack
  timestep = 10;
  val_trig = 500;
}

int Seg::pos_led(int i){
  if (!reverse){
    return start_strip + i;
  }else{
    return end_strip -1 - i;
  }
}


///////////////////////////////////////////
// Color manipulation /////////////////////
///////////////////////////////////////////

/**
 * Set default color
 * and fill @col_table from 0 to @lightness
 */
void Seg::set_color(unsigned int hue, unsigned int saturation, unsigned int lightness){
  logln("[seg] set color");
  for (int i=0; i<100; i++){
    int light = int(map(i, 0, 100, 0, lightness));
    col_table[i] = makeColor(hue, saturation, light);
  }
}

/**
 * Set & show the color of the segment (hsl)
 */
void Seg::set_full_hsl(unsigned int hue, unsigned int saturation, unsigned int lightness){
  int color = makeColor(hue, saturation, lightness);

  for (int i = 0; i< _seg_length; i++){
    leds->setPixel(pos_led(i), color);
  }
  leds->show();
}

/**
 * Set the color of the segment
 */
void Seg::set_full(int color){
  for (int i = 0; i< _seg_length; i++){
    leds->setPixel(pos_led(i), color);
  }
}


///////////////////////////////////////////
// mode 1 : FADE OUT //////////////////////
///////////////////////////////////////////

void Seg::set_mode_fadeout(int val_in){
  logln("[seg] fadeout start");
  pos = 0;
  reverse = false;
  val = val_in;
  start_time = millis();
}

boolean Seg::update_fadeout(){
  if (start_time > 0){
    unsigned long now = millis();
    unsigned long dur = now - start_time;
    pos = float(dur) / float(val);
    if (pos > 1){
      pos = 1;
      start_time = 0;
      logln("[seg] fadeout end");
    }
    int cur_pos = int(pos*99.0);
    int cur_col = col_table[99 - cur_pos];

    // update only if pos_col changed
    if (cur_col != pos_col){
      set_full(cur_col);
      pos_col = cur_col;

      debug("[seg][fadeout] pos ");
      debugln(cur_col);

      return true;
    }

    return false;
  }
  return start_time;
}


///////////////////////////////////////////
// mode 2 : FADE IN ///////////////////////
///////////////////////////////////////////

void Seg::set_mode_fadein(int val_in){
  logln("[seg] fadein start");
  pos = 0;
  val = val_in;
  reverse = false;
  start_time = millis();
}

boolean Seg::update_fadein(){
  if (start_time > 0){
    unsigned long now = millis();
    unsigned long dur = now - start_time;
    pos = float(dur) / float(val);
    if (pos > 1){
      logln("[seg] fadein end");
      pos = 1;
      start_time = 0;
    }
    int cur_col = col_table[int(pos*99.0)];

    // update only if pos_col changed
    if (cur_col != pos_col){
      set_full(cur_col);
      pos_col = cur_col;

      debug("[seg][fadein] pos");
      debugln(pos_col);

      return true;
    }

    return false;
  }
  return start_time;
}


///////////////////////////////////////////
// mode 3 : SNAKE /////////////////////////
///////////////////////////////////////////

void Seg::set_mode_snake(int val_in){
  logln("[seg] snake start");
  if (val_in < 0){
    val = -val_in;
    reverse = true;
  }else{
    val = val_in;
    reverse = false;
  }
  pos = 0;
  start_time = millis();
}

boolean Seg::update_snake(){
  if (start_time > 0){
    unsigned long now = millis();
    unsigned long dur = now - start_time;
    pos = float(dur) / float(val);
    if (pos > 1){
      logln("[seg] snake end ");
      pos = 1;
      start_time = 0;
    }

    int new_pos_pix = int(pos * ( _seg_length - 1 ));
    if (new_pos_pix != pos_pix){
      for (int i = pos_pix; i<new_pos_pix;i++){
        leds->setPixel(pos_led(i), col_table[99]);
      }
      pos_pix = new_pos_pix;

      debug("[seg][snake] pos ");
      debugln(pos_pix);

      return true;
    }
    return false;
  }
  return start_time;
}


///////////////////////////////////////////
// mode 4 : STACK  ////////////////////////
///////////////////////////////////////////

void Seg::set_mode_stack(int val_in){
  if (val_in < 0){
    val = -val_in;
    reverse = true;
  }else{
    val = val_in;
    reverse = false;
  }
  timestep = val / _seg_length;
  log("[seg] stack timestep ");
  logln(timestep);
  last_trig = millis();
  for (int i=0; i< _seg_length; i++){
    lum[i] = 0;
  }
}

boolean Seg::update_stack(){
  unsigned long now = millis();
  unsigned long dur = now - last_trig;

  // update only at a certain rate
  if (dur > timestep){
    last_trig = now;
    boolean changed = false;

    // move each color by one pixel and set the pixel color
    for (int i = _seg_length-1; i > 0; i--){
      lum[i] = lum[i-1];
      leds->setPixel(pos_led(i), lum[i]);
      changed = changed || ( lum[i] != 0 );
    }

    // if trig, set the first pixel color
    if (start_time != 0){
      unsigned long dur_start = now - start_time;
      pos = float(dur_start) / float(val_trig);
      if (pos > 1){
        pos = 1;
        start_time = 0;
      }
      lum[0] = col_table[99 - int(pos * 99)];
    }else{
      lum[0] = 0;
    }

    changed = changed || ( lum[0] != 0 );

    #if DEBUG == 3
    if (changed && lum[_seg_length-1] != 0){
      debug("[seg][stack] val ");
      debugln(lum[_seg_length - 1]);
    }

    #endif
    // debug
    if (changed){
      return true;
    }
  }
  return false;
}

void Seg::trig_stack(int val_in){
  val_trig = val_in;
  start_time = millis();
}


///////////////////////////////////////////
// Global setup and actions ///////////////
///////////////////////////////////////////

void Seg::set_seg_num(int seg_num_in){
  seg_num = seg_num_in;
}

void Seg::set_mode(int mode_in, int val_in){
  mode = mode_in;

  log("[seg] change mode: ");
  logln(mode_in);

  if(mode == 1){
    set_mode_fadeout(val_in);
  } else if(mode == 2){
    set_mode_fadein(val_in);
  } else if(mode == 3){
    set_mode_snake(val_in);
  } else if(mode == 4){
    set_mode_stack(val_in);
  }
}


boolean Seg::update(){
  if (mode == 1){
    return update_fadeout();
  }else if (mode == 2){
    return update_fadein();
  }else if (mode == 3){
    return update_snake();
  }else if (mode == 4){
    return update_stack();
  }
  return false;
}

void Seg::trig(int val_in){
  log("[seg] main trig ");
  logln(mode);
  if (mode == 4){
    trig_stack(val_in);
  }
}
