#ifndef log_h
#define log_h

#define DEBUG 1

#if DEBUG == 1
#define warning(x) Serial.print(x)
#define warningln(x) Serial.println(x)
#define log(x)
#define logln(x)
#define debug(x)
#define debugln(x)
#elif DEBUG == 2
#define warning(x) Serial.print(x)
#define warningln(x) Serial.println(x)
#define log(x) Serial.print(x)
#define logln(x) Serial.println(x)
#define debug(x)
#define debugln(x)
#elif DEBUG == 3
#define warning(x) Serial.print(x)
#define warningln(x) Serial.println(x)
#define log(x) Serial.print(x)
#define logln(x) Serial.println(x)
#define debug(x) Serial.print(x)
#define debugln(x) Serial.println(x)
#else
#define warning(x)
#define warningln(x)
#define log(x)
#define logln(x)
#define debug(x)
#define debugln(x)
#endif

#endif
